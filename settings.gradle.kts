pluginManagement {
    val androidPluginVersion: String by settings
    val jetbrainsKotlinVersion: String by settings
    plugins {
        id("com.android.application") version androidPluginVersion
        id("com.android.library") version androidPluginVersion
        kotlin("android") version jetbrainsKotlinVersion
        kotlin("jvm") version jetbrainsKotlinVersion
        kotlin("kapt") version jetbrainsKotlinVersion
        id("org.jetbrains.dokka") version jetbrainsKotlinVersion
    }

    repositories {
        gradlePluginPortal()
        google()
    }
}

dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        maven {
            url = uri("https://jitpack.io")
        }
    }
}

rootProject.name = "Glassfy"
include(":app")
include(":glassfy")
