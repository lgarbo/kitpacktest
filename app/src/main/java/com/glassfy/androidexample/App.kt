package com.glassfy.androidexample

import android.app.Application
import android.widget.Toast
import io.glassfy.androidsdk.Glassfy
import io.glassfy.androidsdk.GlassfyError
import io.glassfy.androidsdk.InitializeCallback
import io.glassfy.androidsdk.LogLevel

class App : Application() {

    companion object {
        private val TAG by lazy { App::class.java.simpleName }
    }

    override fun onCreate() {
        super.onCreate()

        initializeGlassfy()         // com.glassfy.androidexample
//        initializeGlassfyBis()      // com.glassfy.androidexamplebis
    }

    private fun initializeGlassfy() {
        Glassfy.setLogLevel(LogLevel.DEBUG)
        Glassfy.initialize(
            this,
            "8NOV6FQBHHRJ4F9N2HV0HUXOQ5LL0QA3/OBMVM0E1ZZQ77TZOQNOETSHHPQOZX6LQ",
            false,
            object : InitializeCallback {
                override fun onSuccess(result: Unit) {
                    println("$TAG - initialize success")
                }

                override fun onError(error: GlassfyError) {
                    Toast
                        .makeText(applicationContext, "Exception ${error.description}  + ${error.debug}", Toast.LENGTH_LONG)
                        .show()
                }
            }
        )
    }

    private fun initializeGlassfyBis() {
        Glassfy.initialize(
            this,
            "f7df0262fd3c4db3b4a7a65bd4862d5c",
            false,
            object : InitializeCallback {
                override fun onSuccess(result: Unit) {
                    println("$TAG - initialize success")
                }

                override fun onError(error: GlassfyError) {
                    Toast
                        .makeText(applicationContext, "Exception ${error.description}  + ${error.debug}", Toast.LENGTH_LONG)
                        .show()
                }
            }
        )
    }
}