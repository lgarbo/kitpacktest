package com.glassfy.androidexample.main.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.glassfy.androidexample.R
import io.glassfy.androidsdk.model.HistoryPurchase

class HistoryPurchasesListAdapter(private val onClick: (HistoryPurchase) -> Unit) :
    ListAdapter<HistoryPurchase, HistoryPurchasesListAdapter.ViewHolder>(HistoryDiffCallback) {
    class ViewHolder(itemView: View, val onClick: (HistoryPurchase) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.name)
        private var currentPurchase: HistoryPurchase? = null

        init {
            itemView.setOnClickListener {
                currentPurchase?.let {
                    onClick(it)
                }
            }
        }

        fun bind(o: HistoryPurchase) {
            currentPurchase = o
            nameTextView.text = o.skus.joinToString(", ") + " - " + o.quantity
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_material_text, parent, false)
        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }
}

object HistoryDiffCallback : DiffUtil.ItemCallback<HistoryPurchase>() {
    override fun areItemsTheSame(oldItem: HistoryPurchase, newItem: HistoryPurchase): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: HistoryPurchase, newItem: HistoryPurchase): Boolean {
        return oldItem.hashCode == newItem.hashCode
    }
}
