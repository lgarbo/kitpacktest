package com.glassfy.androidexample.main.model

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.glassfy.androidsdk.Glassfy
import io.glassfy.androidsdk.model.HistoryPurchase
import io.glassfy.androidsdk.model.Offering
import io.glassfy.androidsdk.model.Permission
import io.glassfy.androidsdk.model.Purchase
import kotlinx.coroutines.launch

class MainViewModel : ViewModel() {
    private val _offerings = MutableLiveData<List<Offering>>()
    val offerings: LiveData<List<Offering>> = _offerings

    private val _historyPurchases = MutableLiveData<List<HistoryPurchase>>()
    val historyPurchases: LiveData<List<HistoryPurchase>> = _historyPurchases

    private val _purchases = MutableLiveData<List<Purchase>>()
    val purchases: LiveData<List<Purchase>> = _purchases

    private val _permissions = MutableLiveData<List<Permission>>()
    val permissions: LiveData<List<Permission>> = _permissions

    private val _error = MutableLiveData<String?>()
    val error: LiveData<String?> = _error


    fun clearError() {
        _error.value = null
    }

    fun offeringWithIdentifier(identifier: String): Offering? {
        return _offerings.value?.find { it.offeringId == identifier }
    }

    /**
     * Updates offerings
     */
    fun updateOfferings() {
        _offerings.value = emptyList()
        _error.value = null

        viewModelScope.launch {
            Glassfy.offerings { result, error ->
                _offerings.value = result?.all ?: emptyList()
                _error.value = error?.description
            }
        }
    }

    /**
     * Updates History purchases
     */
    fun updateHistoryPurchases() {
        _historyPurchases.value = emptyList()
        _error.value = null

        viewModelScope.launch {
            Glassfy.historyPurchases { result, error ->
                _historyPurchases.value = result ?: emptyList()
                _error.value = error?.description
                if (_error.value == null && _historyPurchases.value.isNullOrEmpty()) {
                    _error.value = "Empty List"
                }
            }
        }
    }

    /**
     * Updates Active Purchases
     */
    fun updateActivePurchases() {
        _purchases.value = emptyList()
        _error.value = null

        viewModelScope.launch {
            Glassfy.activePurchases { result, error ->
                _purchases.value = result ?: emptyList()
                _error.value = error?.description
                if (_error.value == null && _purchases.value.isNullOrEmpty()) {
                    _error.value = "Empty List"
                }
            }
        }
    }


    /**
     * Updates Permissions
     */
    fun updatePermissions() {
        _permissions.value = emptyList()
        _error.value = null

        viewModelScope.launch {

            Glassfy.permissions { result, error ->
                _permissions.value = result?.all ?: emptyList()
                _error.value = error?.description
                if (_error.value == null && _permissions.value.isNullOrEmpty()) {
                    _error.value = "Empty List"
                }
            }
        }
    }


    /**
     * Updates the offerings
     */
//    fun upgradePurchase(a: Activity?, p: Purchase) {
//        if (a == null) {
//            println("Missing activity")
//            return
//        }
////        viewModelScope.launch {
//        Glassfy.offerings(object: OfferingsCallback {
//            override fun onSuccess(result: Offerings) {
//                val ofer_name = "subscription_articles"
//                val s = result.offers.find { it.identifier == ofer_name }?.skus?.filter { it.productid != p.skus.firstOrNull() }?.random()
//                if (s == null) {
//                    Toast.makeText(a, "Can t find offers $ofer_name!", Toast.LENGTH_SHORT).show()
//                    return
//                }
//
//                Glassfy.purchase(
//                    a,
//                    s,
//                    SubscriptionUpdate(
//                        p.purchaseToken,
//                        ProrationMode.IMMEDIATE_WITH_TIME_PRORATION
//                    ),
//                    object : PurchaseCallback {
//                        override fun onError(error: GlassfyError) {
//                            Toast.makeText(a, error.description, Toast.LENGTH_SHORT).show()
//                        }
//
//                        override fun onSuccess(result: Permissions) {
//                            Toast.makeText(a, "Success!", Toast.LENGTH_SHORT).show()
//                        }
//                    })
//            }
//
//            override fun onError(error: GlassfyError) {
//                Toast.makeText(a, error.description, Toast.LENGTH_SHORT).show()
//            }
//        })
//
//    }
}