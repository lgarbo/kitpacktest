package com.glassfy.androidexample.main.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.glassfy.androidexample.R
import com.glassfy.androidexample.databinding.FragmentMainBinding
import io.glassfy.androidsdk.Glassfy
import io.glassfy.androidsdk.GlassfyError
import io.glassfy.androidsdk.PermissionsCallback
import io.glassfy.androidsdk.model.Permissions

class MainFragment : Fragment(R.layout.fragment_main) {

    private var _binding: FragmentMainBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    companion object {
        fun newInstance() = MainFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMainBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.btnShowOffering.setOnClickListener {
            showOffering()
        }

        binding.btnShowActivePurchases.setOnClickListener {
            showActivePurchase()
        }

        binding.btnShowHistoryPurchases.setOnClickListener {
            showHistoryPurchase()
        }

        binding.btnShowPermissions.setOnClickListener {
            showPermissions()
        }

        binding.btnRestore.setOnClickListener {
            restore()
        }

        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun restore() {
        Glassfy.restore(object : PermissionsCallback {
            override fun onSuccess(result: Permissions) {
                Toast.makeText(activity, "Restore Success", Toast.LENGTH_SHORT).show()
            }

            override fun onError(error: GlassfyError) {
                Toast
                    .makeText(
                        activity,
                        "Exception ${error.description}  - ${error.debug}",
                        Toast.LENGTH_LONG
                    )
                    .show()
            }
        })
    }

    private fun showOffering() {
        findNavController().navigate(R.id.action_mainFragment_to_offeringListFragment2)
    }

    private fun showActivePurchase() {
        findNavController().navigate(R.id.action_mainFragment_to_activePurchasesListFragment)
    }

    private fun showHistoryPurchase() {
        findNavController().navigate(R.id.action_mainFragment_to_historyPurchasesListFragment)
    }

    private fun showPermissions() {
        findNavController().navigate(R.id.action_mainFragment_to_permissionsListFragment)
    }
}