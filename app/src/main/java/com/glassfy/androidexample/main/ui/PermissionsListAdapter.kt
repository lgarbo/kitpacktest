package com.glassfy.androidexample.main.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.glassfy.androidexample.R
import io.glassfy.androidsdk.model.Permission

class PermissionsListAdapter(private val onClick: (Permission) -> Unit) :
    ListAdapter<Permission, PermissionsListAdapter.ViewHolder>(PermissionDiffCallback) {
    class ViewHolder(itemView: View, val onClick: (Permission) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.name)
        private var currentPermission: Permission? = null

        init {
            itemView.setOnClickListener {
                currentPermission?.let {
                    onClick(it)
                }
            }
        }

        fun bind(o: Permission) {
            currentPermission = o
            nameTextView.text = o.permissionId + " - " + o.entitlement
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_material_text, parent, false)
        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }
}

private object PermissionDiffCallback : DiffUtil.ItemCallback<Permission>() {
    override fun areItemsTheSame(oldItem: Permission, newItem: Permission): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Permission, newItem: Permission): Boolean {
        return oldItem.permissionId == newItem.permissionId
    }
}