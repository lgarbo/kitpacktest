package com.glassfy.androidexample.main.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.glassfy.androidexample.R
import io.glassfy.androidsdk.model.Purchase

class ActivePurchasesListAdapter(private val onClick: (Purchase) -> Unit) :
    ListAdapter<Purchase, ActivePurchasesListAdapter.ViewHolder>(PurchaseDiffCallback) {
    class ViewHolder(itemView: View, val onClick: (Purchase) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.name)
        private var currentPurchase: Purchase? = null

        init {
            itemView.setOnClickListener {
                currentPurchase?.let {
                    onClick(it)
                }
            }
        }

        fun bind(o: Purchase) {
            currentPurchase = o
            nameTextView.text = o.skus.joinToString()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_material_text, parent, false)
        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }
}

private object PurchaseDiffCallback : DiffUtil.ItemCallback<Purchase>() {
    override fun areItemsTheSame(oldItem: Purchase, newItem: Purchase): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Purchase, newItem: Purchase): Boolean {
        return oldItem.hashCode == newItem.hashCode
    }
}
