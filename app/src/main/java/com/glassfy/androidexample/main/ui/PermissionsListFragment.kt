package com.glassfy.androidexample.main.ui

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import com.glassfy.androidexample.R
import com.glassfy.androidexample.databinding.FragmentListErrorProgressBinding
import com.glassfy.androidexample.main.model.MainViewModel
import io.glassfy.androidsdk.model.Permission

class PermissionsListFragment : Fragment(R.layout.fragment_list_error_progress) {

    private var _binding: FragmentListErrorProgressBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val sharedViewModel: MainViewModel by activityViewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding = FragmentListErrorProgressBinding.inflate(inflater, container, false)
        _binding = fragmentBinding

        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val adapter = PermissionsListAdapter { o -> adapterOnClick(o) }
        binding.rv.adapter = adapter

        sharedViewModel.clearError()

        sharedViewModel.permissions.observe(viewLifecycleOwner) {
            hideProgressBar()
            hideErrorMessage()
            it.orEmpty().let {
                adapter.submitList(it)
            }
        }

        sharedViewModel.error.observe(viewLifecycleOwner) {
            hideProgressBar()
            hideErrorMessage()
            it?.let {
                showErrorMessage(it)
            }
        }

        binding.itemErrorMessage.btnRetry.setOnClickListener {
            hideErrorMessage()
            showProgressBar()
            sharedViewModel.updatePermissions()
        }

        if (sharedViewModel.permissions.value.isNullOrEmpty()) {
            showProgressBar()
            sharedViewModel.updatePermissions()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.refresh_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_offering_refresh -> {
                hideErrorMessage()
                showProgressBar()
                sharedViewModel.updatePermissions()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun adapterOnClick(o: Permission) {
//        sharedViewModel.upgradePurchase(activity, o)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideErrorMessage() {
        binding.itemErrorMessage.root.visibility = View.INVISIBLE
    }

    private fun showErrorMessage(message: String) {
        binding.itemErrorMessage.root.visibility = View.VISIBLE
        binding.itemErrorMessage.tvErrorMessage.text = message
    }
}