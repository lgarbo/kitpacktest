package com.glassfy.androidexample.main.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.glassfy.androidexample.R
import io.glassfy.androidsdk.model.Offering

class OfferingListAdapter(private val onClick: (Offering) -> Unit) :
    ListAdapter<Offering, OfferingListAdapter.ViewHolder>(OfferingDiffCallback) {
    class ViewHolder(itemView: View, val onClick: (Offering) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.name)
        private var currentOffering: Offering? = null

        init {
            itemView.setOnClickListener {
                currentOffering?.let {
                    onClick(it)
                }
            }
        }

        fun bind(o: Offering) {
            currentOffering = o
            nameTextView.text = o.offeringId
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_material_text, parent, false)
        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }
}

object OfferingDiffCallback : DiffUtil.ItemCallback<Offering>() {
    override fun areItemsTheSame(oldItem: Offering, newItem: Offering): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Offering, newItem: Offering): Boolean {
        return oldItem.offeringId == newItem.offeringId
    }
}
