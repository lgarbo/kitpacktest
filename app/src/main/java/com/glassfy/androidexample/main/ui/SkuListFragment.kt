package com.glassfy.androidexample.main.ui


import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.glassfy.androidexample.R
import com.glassfy.androidexample.databinding.FragmentListErrorProgressBinding
import com.glassfy.androidexample.main.model.MainViewModel
import io.glassfy.androidsdk.Glassfy
import io.glassfy.androidsdk.GlassfyError
import io.glassfy.androidsdk.PurchaseCallback
import io.glassfy.androidsdk.model.Sku
import io.glassfy.androidsdk.model.Transaction
import kotlinx.coroutines.launch


class SkuListFragment : Fragment(R.layout.fragment_list_error_progress) {


    private var _binding: FragmentListErrorProgressBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val sharedViewModel: MainViewModel by activityViewModels()

    companion object {
        private val TAG = "SkuListFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding = FragmentListErrorProgressBinding.inflate(inflater, container, false)
        _binding = fragmentBinding

        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapter = SkuListAdapter { o -> adapterOnClick(o) }
        binding.rv.adapter = adapter

        sharedViewModel.clearError()

        val offeringId = arguments?.getString("selected_offering")
        offeringId?.let {
            sharedViewModel.offeringWithIdentifier(it)
        }?.also {
            adapter.submitList(it.skus)
        }

        sharedViewModel.offerings.observe(viewLifecycleOwner) { list ->

            if (list.isEmpty()) {
//                showProgressBar()
            } else {
                hideProgressBar()
                hideErrorMessage()
            }

            val o =
                if (offeringId != null) sharedViewModel.offeringWithIdentifier(offeringId) else list.firstOrNull()
            o?.let {
                adapter.submitList(it.skus)
            }
        }

        sharedViewModel.error.observe(viewLifecycleOwner) {

            hideErrorMessage()
            it?.let {
                hideProgressBar()
                showErrorMessage(it)
            }
        }

        binding.itemErrorMessage.btnRetry.setOnClickListener {
            hideErrorMessage()
            showProgressBar()
            sharedViewModel.updateOfferings()
        }

        if (sharedViewModel.offerings.value.isNullOrEmpty()) {
            showProgressBar()
            sharedViewModel.updateOfferings()
        }
    }

    private fun adapterOnClick(o: Sku) {
        activity?.let {
            startPurchase(o, it)
        }
    }

    private fun startPurchase(o: Sku, activity: Activity) {
        lifecycleScope.launch {
            Glassfy.purchase(activity, o, callback =  object:PurchaseCallback {
                override fun onSuccess(result: Transaction) {
                    Toast.makeText(activity, "Success!", Toast.LENGTH_SHORT).show()
                }

                override fun onError(error: GlassfyError) {
                    Toast
                        .makeText(
                            activity,
                            "Exception ${error.description}  - ${error.debug}",
                            Toast.LENGTH_LONG
                        )
                        .show()
                }
            })
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideErrorMessage() {
        binding.itemErrorMessage.root.visibility = View.INVISIBLE
    }

    private fun showErrorMessage(message: String) {
        binding.itemErrorMessage.root.visibility = View.VISIBLE
        binding.itemErrorMessage.tvErrorMessage.text = message
    }
}