package com.glassfy.androidexample.main.ui

import android.os.Bundle
import android.view.*
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.glassfy.androidexample.R
import com.glassfy.androidexample.databinding.FragmentListErrorProgressBinding
import com.glassfy.androidexample.main.model.MainViewModel
import io.glassfy.androidsdk.model.Offering

class OfferingListFragment : Fragment(R.layout.fragment_list_error_progress) {
    private var _binding: FragmentListErrorProgressBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    // Use the 'by activityViewModels()' Kotlin property delegate from the fragment-ktx artifact
    private val sharedViewModel: MainViewModel by activityViewModels()

    companion object {
        private val TAG = "OfferingListFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding = FragmentListErrorProgressBinding.inflate(inflater, container, false)
        _binding = fragmentBinding

        return fragmentBinding.root
    }

    private fun adapterOnClick(o: Offering) {
        val bundle = bundleOf("selected_offering" to o.offeringId)

        findNavController().navigate(R.id.action_offeringListFragment2_to_skuListFragment2, bundle)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val adapter = OfferingListAdapter { o -> adapterOnClick(o) }
        binding.rv.adapter = adapter
        sharedViewModel.clearError()

        sharedViewModel.offerings.observe(viewLifecycleOwner) {
            if (it.isNotEmpty()) {
                hideProgressBar()
                hideErrorMessage()
            }

            adapter.submitList(it)
        }

        sharedViewModel.error.observe(viewLifecycleOwner) {
            hideErrorMessage()
            it?.let {
                hideProgressBar()
                showErrorMessage(it)
            }
        }

        binding.itemErrorMessage.btnRetry.setOnClickListener {
            hideErrorMessage()
            showProgressBar()
            sharedViewModel.updateOfferings()
        }

        if (sharedViewModel.offerings.value.isNullOrEmpty()) {
            showProgressBar()
            sharedViewModel.updateOfferings()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)

        inflater.inflate(R.menu.refresh_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_offering_refresh -> {
                hideErrorMessage()
                showProgressBar()
                sharedViewModel.updateOfferings()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideErrorMessage() {
        binding.itemErrorMessage.root.visibility = View.INVISIBLE
    }

    private fun showErrorMessage(message: String) {
        binding.itemErrorMessage.root.visibility = View.VISIBLE
        binding.itemErrorMessage.tvErrorMessage.text = message
    }
}