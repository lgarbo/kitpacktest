package com.glassfy.androidexample.main.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.glassfy.androidexample.R
import io.glassfy.androidsdk.model.Sku

class SkuListAdapter(private val onClick: (Sku) -> Unit) :
    ListAdapter<Sku, SkuListAdapter.ViewHolder>(SkuDiffCallback) {
    class ViewHolder(itemView: View, val onClick: (Sku) -> Unit) :
        RecyclerView.ViewHolder(itemView) {
        private val nameTextView: TextView = itemView.findViewById(R.id.name)
        private var currentSku: Sku? = null

        init {
            itemView.setOnClickListener {
                currentSku?.let {
                    onClick(it)
                }
            }
        }

        fun bind(o: Sku) {
            currentSku = o
            nameTextView.text = o.skuId
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_material_text, parent, false)
        return ViewHolder(view, onClick)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position).let {
            holder.bind(it)
        }
    }
}

object SkuDiffCallback : DiffUtil.ItemCallback<Sku>() {
    override fun areItemsTheSame(oldItem: Sku, newItem: Sku): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: Sku, newItem: Sku): Boolean {
        return oldItem.skuId == newItem.skuId
    }
}