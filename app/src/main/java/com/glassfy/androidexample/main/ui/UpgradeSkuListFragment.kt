package com.glassfy.androidexample.main.ui

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.glassfy.androidexample.R
import com.glassfy.androidexample.databinding.FragmentListErrorProgressBinding
import com.glassfy.androidexample.main.model.MainViewModel
import io.glassfy.androidsdk.Glassfy
import io.glassfy.androidsdk.GlassfyError
import io.glassfy.androidsdk.PurchaseCallback
import io.glassfy.androidsdk.model.ProrationMode
import io.glassfy.androidsdk.model.Sku
import io.glassfy.androidsdk.model.SubscriptionUpdate
import io.glassfy.androidsdk.model.Transaction
import kotlinx.coroutines.launch


class UpgradeSkuListFragment : Fragment(R.layout.fragment_list_error_progress) {

    // default
    private var proratedMode = menuItemToProrated(R.id.menu_prorated_immediate_time_proration)

    private var _binding: FragmentListErrorProgressBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private val sharedViewModel: MainViewModel by activityViewModels()

    companion object {
        private val TAG = "SkuListFragment"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val fragmentBinding = FragmentListErrorProgressBinding.inflate(inflater, container, false)
        _binding = fragmentBinding

        return fragmentBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setHasOptionsMenu(true)

        val adapter = SkuListAdapter { o -> adapterOnClick(o) }
        binding.rv.adapter = adapter

        sharedViewModel.clearError()

        sharedViewModel.offerings.observe(viewLifecycleOwner) { list ->

            if (list.isEmpty()) {
//                showProgressBar()
            } else {
                hideProgressBar()
                hideErrorMessage()
            }

            list?.flatMap { it.skus }?.let {
                adapter.submitList(it)
            }
        }

        sharedViewModel.error.observe(viewLifecycleOwner) {

            hideErrorMessage()
            it?.let {
                hideProgressBar()
                showErrorMessage(it)
            }
        }

        binding.itemErrorMessage.btnRetry.setOnClickListener {
            hideErrorMessage()
            showProgressBar()
            sharedViewModel.updateOfferings()
        }

        if (sharedViewModel.offerings.value.isNullOrEmpty()) {
            showProgressBar()
            sharedViewModel.updateOfferings()
        }
    }

    private fun adapterOnClick(o: Sku) {
        arguments?.getString("selected_purchase")?.let { sku ->
            activity?.let {
                startUpgrade(o, sku, proratedMode, it)
            }
        } ?: Toast.makeText(
            activity,
            "Missing purchase toke arg, restart from MainAcivity",
            Toast.LENGTH_LONG
        ).show()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.prorated_menu, menu)
        menu.findItem(R.id.menu_prorated_immediate_time_proration).isChecked = true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.groupId) {
            R.id.prorated_group -> {
                item.isChecked = !item.isChecked

                proratedMode = menuItemToProrated(item.itemId)

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun startUpgrade(
        o: Sku,
        originalSku: String,
        proratedMode: Int,
        activity: FragmentActivity
    ) {
        lifecycleScope.launch {
            Glassfy.skuWithProductId(originalSku) { s, e ->
                val update = SubscriptionUpdate(
                    s?.skuId ?: "",
                    ProrationMode.fromProrationModeValue(proratedMode)
                )
                Glassfy.purchase(activity, o, update, object : PurchaseCallback {
                    override fun onSuccess(result: Transaction) {
                        Toast.makeText(activity, "Success!", Toast.LENGTH_SHORT).show()
                    }

                    override fun onError(error: GlassfyError) {
                        Toast
                            .makeText(
                                activity,
                                "Exception ${error.description}  - ${error.debug}",
                                Toast.LENGTH_LONG
                            )
                            .show()
                    }
                })
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun hideProgressBar() {
        binding.progressBar.visibility = View.INVISIBLE
    }

    private fun showProgressBar() {
        binding.progressBar.visibility = View.VISIBLE
    }

    private fun hideErrorMessage() {
        binding.itemErrorMessage.root.visibility = View.INVISIBLE
    }

    private fun showErrorMessage(message: String) {
        binding.itemErrorMessage.root.visibility = View.VISIBLE
        binding.itemErrorMessage.tvErrorMessage.text = message
    }

    private fun menuItemToProrated(menuItemId: Int): Int {
        return when (menuItemId) {
            R.id.menu_unknown -> 0
            R.id.menu_prorated_immediate_time_proration -> 1
            R.id.menu_prorated_immediate_charge_prorated -> 2
            R.id.menu_prorated_immediate_no_proration -> 3
            R.id.menu_prorated_deferred -> 4
            R.id.menu_prorated_immediate_charge_full -> 5

            else -> throw Error("Unknow prorated menu")
        }
    }
}