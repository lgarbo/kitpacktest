package io.glassfy.androidsdk

import android.content.Context
import io.glassfy.androidsdk.internal.network.model.OfferingDto
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import java.util.concurrent.CountDownLatch
import java.util.concurrent.TimeUnit

class GlassfyTest {
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")
    private var mainThreadId = -1L
    private val lock = CountDownLatch(1)

    @Mock
    private lateinit var mockContext: Context

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)
        runBlocking(Dispatchers.Main) {
            mainThreadId = Thread.currentThread().id
        }
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain() // reset the main dispatcher to the original Main dispatcher
        mainThreadSurrogate.close()
    }

    @Test
    fun `Check SDK version`() {
        assertEquals(Glassfy.sdkVersion, "1.0.0")
    }

    @Test
    fun `Test Purchase`() {
        Glassfy.testpurchase()
        lock.await(5000L, TimeUnit.MILLISECONDS)
    }


    @Test
    fun `Offering no initialize`() {
        var _offerings: List<OfferingDto>? = null
        var _error: GlassfyError? = null
        Glassfy.offerings(object : OfferingsCallback {
            override fun onSuccess(result: List<OfferingDto>) {
                assertEquals(Thread.currentThread().id, mainThreadId)

                _offerings = result
                lock.countDown()
            }

            override fun onError(error: GlassfyError) {
                assertEquals(Thread.currentThread().id, mainThreadId)

                _error = error
                lock.countDown()
            }
        })
        lock.await(5000L, TimeUnit.MILLISECONDS)
        assertNull(_offerings)
        assertNotNull(_error)
    }
}
