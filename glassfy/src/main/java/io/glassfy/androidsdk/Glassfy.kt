package io.glassfy.androidsdk

import android.app.Activity
import android.content.Context
import io.glassfy.androidsdk.internal.GManager
import io.glassfy.androidsdk.internal.network.model.utils.Resource
import io.glassfy.androidsdk.model.*
import kotlinx.coroutines.*

//ToDo test app integraton without INTERNET, ACCESS_NETWORK_STATE, ACCESS_WIFI_STATE permissions or billingclient and WatcherMode

object Glassfy {
    const val sdkVersion = "1.0.0"

    @JvmStatic
    @JvmOverloads
    fun initialize(
        ctx: Context,
        apiKey: String,
        watcherMode: Boolean = false,
        callback: InitializeCallbackKtx?
    ) {
        initialize(ctx, apiKey, watcherMode, object : InitializeCallback {
            override fun onSuccess(result: Unit) {
                callback?.let { it(result, null) }
            }

            override fun onError(error: GlassfyError) {
                callback?.let { it(null, error) }
            }
        })
    }

    @JvmStatic
    @JvmOverloads
    fun initialize(
        ctx: Context,
        apiKey: String,
        watcherMode: Boolean = false,
        callback: InitializeCallback?
    ) {
        if (callback != null) {
            customScope.runAndPostResult(callback) { manager.initialize(ctx, apiKey, watcherMode) }
        } else {
            customScope.runNoResult { manager.initialize(ctx, apiKey, watcherMode) }
        }
    }

    @JvmStatic
    fun setPurchaseDelegate(delegate: PurchaseDelegate) {
        customScope.runNoResult { manager.setPurchaseDelegate(delegate) }
    }

    @JvmStatic
    fun setLogLevel(level: LogLevel) {
        customScope.runNoResult { manager.setLogLevel(level) }
    }

    @JvmStatic
    fun offerings(callback: OfferingsCallbackKtx) {
        offerings(object : OfferingsCallback {
            override fun onSuccess(result: Offerings) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun offerings(callback: OfferingsCallback) {
        customScope.runAndPostResult(callback) { manager.offerings() }
    }

    @JvmStatic
    fun sku(identifier: String, callback: SkuCallbackKtx) {
        sku(identifier, object : SkuCallback {
            override fun onSuccess(result: Sku) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun sku(identifier: String, callback: SkuCallback) {
        customScope.runAndPostResult(callback) { manager.sku(identifier) }
    }

    @JvmStatic
    fun skuWithProductId(identifier: String, callback: SkuCallbackKtx) {
        skuWithProductId(identifier, object : SkuCallback {
            override fun onSuccess(result: Sku) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun skuWithProductId(identifier: String, callback: SkuCallback) {
        customScope.runAndPostResult(callback) { manager.skuWithProductId(identifier) }
    }

    @JvmStatic
    fun activePurchases(callback: PurchasesCallbackKtx) {
        activePurchases(object : PurchasesCallback {
            override fun onSuccess(result: List<Purchase>?) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun activePurchases(callback: PurchasesCallback) {
        customScope.runAndPostResult(callback) { manager.activePurchases() }
    }

    @JvmStatic
    fun historyPurchases(callback: HistoryPurchasesCallbackKtx) {
        historyPurchases(object : HistoryPurchasesCallback {
            override fun onSuccess(result: List<HistoryPurchase>?) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun historyPurchases(callback: HistoryPurchasesCallback) {
        customScope.runAndPostResult(callback) { manager.historyPurchases() }
    }

    @JvmStatic
    fun permissions(callback: PermissionsCallbackKtx) {
        permissions(object : PermissionsCallback {
            override fun onSuccess(result: Permissions) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun permissions(callback: PermissionsCallback) {
        customScope.runAndPostResult(callback) { manager.permissions() }
    }

    @JvmStatic
    fun restore(callback: PermissionsCallbackKtx) {
        restore(object : PermissionsCallback {
            override fun onSuccess(result: Permissions) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    fun restore(callback: PermissionsCallback) {
        customScope.runAndPostResult(callback) { manager.restore() }
    }

    @JvmStatic
    @JvmOverloads
    fun purchase(
        activity: Activity,
        sku: Sku,
        updateSku: SubscriptionUpdate? = null,
        callback: PurchaseCallbackKtx
    ) {
        purchase(activity, sku, updateSku, object : PurchaseCallback {
            override fun onSuccess(result: Transaction) {
                callback(result, null)
            }

            override fun onError(error: GlassfyError) {
                callback(null, error)
            }
        })
    }

    @JvmStatic
    @JvmOverloads
    fun purchase(
        activity: Activity,
        sku: Sku,
        updateSku: SubscriptionUpdate? = null,
        callback: PurchaseCallback
    ) {
        customScope.runAndPostResult(callback) { manager.purchase(activity, sku, updateSku) }
    }


//  UTILS

    internal val manager: GManager by lazy { GManager() }
    internal val customScope by lazy {
        CoroutineScope(
            SupervisorJob() + Dispatchers.IO + CoroutineName(
                "glassfy"
            )
        )
    }

    private fun CoroutineScope.runNoResult(block: suspend CoroutineScope.() -> Unit) {
        launch(block = block)
    }

    private fun <T> CoroutineScope.runAndPostResult(
        callback: GenericCallback<T>,
        block: suspend CoroutineScope.() -> Resource<T>
    ) {
        launch {
            val r = block()
            withContext(Dispatchers.Main) {
                when (r) {
                    is Resource.Success -> {
                        callback.onSuccess(r.data!!)
                    }
                    is Resource.Error -> {
                        callback.onError(r.err!!)
                    }
                }
            }
        }
    }

    private fun <T> CoroutineScope.runAndPostResult(
        callback: GenericKotlinCallback<T>,
        block: suspend CoroutineScope.() -> Resource<T>
    ) {
        launch {
            val r = block()
            withContext(Dispatchers.Main) {
                when (r) {
                    is Resource.Success -> {
                        callback(r.data!!, null)
                    }
                    is Resource.Error -> {
                        callback(null, r.err!!)
                    }
                }
            }
        }
    }
}
