package io.glassfy.androidsdk

import androidx.annotation.UiThread
import io.glassfy.androidsdk.model.*


interface ErrorCallback {
    fun onError(error: GlassfyError)
}

interface GenericCallback<in T> : ErrorCallback {
    fun onSuccess(result: T)
}

typealias GenericKotlinCallback<T> = (result: T?, err: GlassfyError?) -> Unit

@FunctionalInterface
@UiThread
interface PurchaseCallback : GenericCallback<Transaction>
typealias PurchaseCallbackKtx = GenericKotlinCallback<Transaction>

@FunctionalInterface
@UiThread
interface OfferingsCallback : GenericCallback<Offerings>
typealias OfferingsCallbackKtx = GenericKotlinCallback<Offerings>

@FunctionalInterface
@UiThread
interface SkuCallback : GenericCallback<Sku>
typealias SkuCallbackKtx = GenericKotlinCallback<Sku>

@FunctionalInterface
@UiThread
interface PurchasesCallback : GenericCallback<List<Purchase>?>
typealias PurchasesCallbackKtx = GenericKotlinCallback<List<Purchase>>

@FunctionalInterface
@UiThread
interface HistoryPurchasesCallback : GenericCallback<List<HistoryPurchase>?>
typealias HistoryPurchasesCallbackKtx = GenericKotlinCallback<List<HistoryPurchase>>

@FunctionalInterface
@UiThread
interface PermissionsCallback : GenericCallback<Permissions>
typealias PermissionsCallbackKtx = GenericKotlinCallback<Permissions>

@FunctionalInterface
@UiThread
interface InitializeCallback : GenericCallback<Unit>
typealias InitializeCallbackKtx = GenericKotlinCallback<Unit>