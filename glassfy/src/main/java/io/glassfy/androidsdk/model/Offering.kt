package io.glassfy.androidsdk.model

data class Offering(val offeringId: String, var skus: List<Sku>)
