plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
    id("maven-publish")
    id("signing")
    id("org.jetbrains.dokka")
}

android {
    compileSdk = 28

    defaultConfig {
        minSdk = 21
        targetSdk = 28
        // buildConfigField "int", "VERSION_CODE", "1000"
        // buildConfigField "String", "VERSION_NAME", "\"1.0.0\""

        consumerProguardFiles("consumer-rules.pro")

        sourceSets.getByName("main") {
            java.srcDir("src/main/java")

        }
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }

    packagingOptions {
        // exclude file used by debugger
        resources.excludes += "DebugProbesKt.bin"
        resources.excludes += "/META-INF/{AL2.0,LGPL2.1}"
    }

    publishing {
        // To publish just one variant, use singleVariant to publish only release
        singleVariant("release")
    }
}

dependencies {
    // Lifecycle
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.4.1")
    implementation("androidx.lifecycle:lifecycle-process:2.4.1")

    // Android Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.6.0")

    // BillingClient
    implementation("com.android.billingclient:billing-ktx:4.1.0")

    // Retrofit + OKHttp + Moshi
    implementation("com.squareup.retrofit2:retrofit:2.9.0")
    implementation("com.squareup.okhttp3:okhttp:4.9.3")
    implementation("com.squareup.moshi:moshi:1.13.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.13.0")
    implementation("com.squareup.okhttp3:logging-interceptor:4.9.3")    //ToDo remove
    implementation("com.squareup.retrofit2:converter-moshi:2.9.0")
    kapt("com.squareup.moshi:moshi-kotlin-codegen:1.13.0")

    testImplementation("junit:junit:4.13.2")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test:1.6.0")

    androidTestImplementation("androidx.test.ext:junit:1.1.3")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.4.0")
}


// Setup documentation build
val dokkaOutputDir = "$buildDir/dokka"
tasks.getByName<org.jetbrains.dokka.gradle.DokkaTask>("dokkaHtml") {
    outputDirectory.set(file(dokkaOutputDir))
}

val deleteDokkaOutputDir by tasks.register<Delete>("deleteDokkaOutputDirectory") {
    delete(dokkaOutputDir)
}

val javadocJar = tasks.register<Jar>("javadocJar") {
    dependsOn(deleteDokkaOutputDir, tasks.dokkaHtml)
    archiveClassifier.set("javadoc")
    from(dokkaOutputDir)
}


// Because the components are created only during the afterEvaluate phase, you must
// configure your publications using the afterEvaluate() lifecycle method.
//afterEvaluate {
//    publishing {
//        repositories {
//            maven {
//                name = "Oss"
//                setUrl {
//                    val repositoryId =
//                        System.getenv("SONATYPE_REPOSITORY_ID")
//                            ?: error("Missing env variable: SONATYPE_REPOSITORY_ID")
//                    "https://s01.oss.sonatype.org/service/local/staging/deployByRepositoryId/${repositoryId}/"
//                }
//                credentials {
//                    username = System.getenv("SONATYPE_USERNAME")
//                    password = System.getenv("SONATYPE_PASSWORD")
//                }
//            }
//            maven {
//                name = "Snapshot"
//                setUrl { "https://s01.oss.sonatype.org/content/repositories/snapshots/" }
//                credentials {
//                    username = System.getenv("SONATYPE_USERNAME")
//                    password = System.getenv("SONATYPE_PASSWORD")
//                }
//            }
//        }
//
//        publications {
//            // Creates a Maven publication called "release".
//            create<MavenPublication>("release") {
//                artifact(javadocJar)    // Add generated docs
////                artifact(sourcesJar)    // Add sources
//
//                // Applies the component for the 'release' build variant.
//                from(components["release"])
//
//                groupId = "io.glassfy"
//                artifactId = "androidSdk"
//                version = "1.0"
//
////                    <packaging>jar</packaging>
//
//
//                val vcs = "https://github.com/glassfy/android-SDK"
//                pom {
//                    name.set("androidsdk")
//                    description.set("Glassfy SDK for android platform")
//                    url.set(vcs)
//
//                    licenses {
//                        license {
//                            name.set("MIT License")
//                            url.set("http://opensource.org/licenses/MIT")
//                        }
//                    }
//                    issueManagement {
//                        system.set("Github")
//                        url.set("${vcs}/issues")
//                    }
//                    developers {
//                        developer {
//                            name.set("Luca Garbolino")
//                            email.set("luca@glassfy.io")
//                            organization.set("Glassfy")
//                            organizationUrl.set("https://glassfy.io")
//                        }
//                    }
//                    scm {
//                        connection.set("scm:git:git://github.com/glassfy/android-SDK.git")
//                        developerConnection.set("scm:git:ssh://github.com:glassfy/android-SDK.git")
//                        url.set("${vcs}/tree/main")
//                    }
//                }
//            }
//        }
//    }
//
//
//
//    signing {
//        val gpgSigningKey = System.getenv("GPG_SIGNING_KEY").replace("\\n", System.getProperty("line.separator"))
//        val gpgSigningPwd = System.getenv("GPG_SIGNING_PWD")
//        useInMemoryPgpKeys(
//            gpgSigningKey,
//            gpgSigningPwd
//        )
//        sign(publishing.publications)
//    }
//}

// Because the components are created only during the afterEvaluate phase, you must
// configure your publications using the afterEvaluate() lifecycle method.
afterEvaluate {
    publishing {
        publications {
            // Creates a Maven publication called "release".
            create<MavenPublication>("release") {
                // Applies the component for the release build variant.
                from(components["release"])

                // You can then customize attributes of the publication as shown below.
                groupId = "gitlab.lgarbo.kitpacktest"
                artifactId = "kitpacktest"
                version = "1.0"
            }
        }
    }
}
