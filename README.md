# Glassfy Android SDK

Is the client for Glassfy: a subscription revenue optimisation infrastructure for mobile applications.

### Getting Started

Check the documentation at [docs.glassfy.io](https://docs.glassfy.io/get-started/quick-start) to learn details on implementing and using Glassfy SDK.

### License

This SDK is available under the MIT license.